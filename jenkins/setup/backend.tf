terraform {
  backend "s3" {
    bucket = "son-node-aws-jenkins-terraform"
    key    = "son-node-aws-jenkins.terraform.tfstate"
    region = "eu-west-1"
  }
}